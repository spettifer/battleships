﻿using AutoFixture.Xunit2;
using Battleships.Domain.Enums;
using Battleships.Domain.Models;
using Battleships.Domain.Services;
using Battleships.Domain.Shared;
using Moq;
using System;
using System.Collections.Generic;
using FluentAssertions;
using Xunit;

namespace Battleships.Domain.UnitTests
{
    public class PlaceShipServiceTests
    {
        [Fact]
        public void InstantiateWithNoRandomGenerator_Should_ThrowArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>("randomGenerator", () => new PlaceShipService(null));
        }

        [Theory]
        [AutoData]
        public void ShipSizeGreaterThanBoardSize_Should_ThrowArgumentException(Mock<IRandomGenerator> mockRandomGenerator, ShipDefinition ship)
        {
            var sut = new PlaceShipService(mockRandomGenerator.Object);

            Assert.Throws<ArgumentOutOfRangeException>("ship", () => sut.PlaceShip(ship, ship.Size - 1));
        }

        [Theory]
        [InlineAutoData(Direction.Column, 10, 1, 5)]
        [InlineAutoData(Direction.Row, 10, 5, 1)]
        public void StartCoordsBelowSeven_Should_ReturnContiguousCoordinatesInAscendingOrder(Direction direction, int boardSize, int startCol, int startRow, Mock<IRandomGenerator> mockRandomGenerator)
        {
            var ship = new ShipDefinition() { FleetIdentifier = 1, Size = 5, Type = ShipType.Battleship };

            // Mock direction
            mockRandomGenerator.Setup(x => x.NextInt(It.Is<int>(i => i == 1), It.Is<int>(i => i == 3)))
                .Returns((int)direction);

            // Mock the start column and row.
            mockRandomGenerator.SetupSequence(x => x.NextInt(It.Is<int>(i => i == 1), It.Is<int>(i => i == boardSize + 1)))
                .Returns(startCol)
                .Returns(startRow);

            var expected = direction == Direction.Column ? new HashSet<string>() { "A5", "A6", "A7", "A8", "A9" } : new HashSet<string>() { "E1", "F1", "G1", "H1", "I1" };

            var sut = new PlaceShipService(mockRandomGenerator.Object);

            var actual = sut.PlaceShip(ship, boardSize);

            actual.Should().BeEquivalentTo(expected);
        }

        [Theory]
        [InlineAutoData(Direction.Column, 10, 1, 7)]
        [InlineAutoData(Direction.Row, 10, 7, 1)]
        public void ValidArguments_With_StartCoordsAboveSix_Should_ReturnContiguousCoordinatesInAscendingOrder(Direction direction, int boardSize, int startCol, int startRow, Mock<IRandomGenerator> mockRandomGenerator)
        {
            // Set up a ship.
            var ship = new ShipDefinition() { FleetIdentifier = 1, Size = 5, Type = ShipType.Battleship };

            // Mock direction
            mockRandomGenerator.Setup(x => x.NextInt(It.Is<int>(i => i == 1), It.Is<int>(i => i == 3)))
                .Returns((int)direction);

            // Mock the start column and row.
            mockRandomGenerator.SetupSequence(x => x.NextInt(It.Is<int>(i => i == 1), It.Is<int>(i => i == boardSize + 1)))
                .Returns(startCol)
                .Returns(startRow);

            var expected = direction == Direction.Column ? new HashSet<string>() { "A7", "A6", "A5", "A4", "A3" } : new HashSet<string>() { "G1", "F1", "E1", "D1", "C1" };

            var sut = new PlaceShipService(mockRandomGenerator.Object);

            var actual = sut.PlaceShip(ship, boardSize);

            actual.Should().BeEquivalentTo(expected);
        }
    }
}
