﻿using Battleships.Domain.Queries;
using Battleships.Domain.Results;
using Battleships.Domain.Services;
using FluentAssertions;
using Xunit;

namespace Battleships.Domain.UnitTests
{
    public class MoveValidatorServiceTests
    {
        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void MissingMove_Should_ReturnFalseAndCorrectMessage(string move)
        {
            // Sut
            var moveValidatorService = new MoveValidatorService();

            var expected = new MoveValidationResult()
                {Message = "You didn't enter a move. Please try again.", IsMoveValid = false, Move = move};

            var actual = moveValidatorService.ValidateMove(new MoveValidationQuery(move));

            actual.Should().BeEquivalentTo(expected, $"a missing or null move is invalid.");
        }

        [Theory]
        [InlineData("a1*")]
        [InlineData("#'")]
        public void InvalidCharacters_Should_ReturnFalseAndCorrectMessage(string move)
        {
            // Sut
            var moveValidatorService = new MoveValidatorService();

            var expected = new MoveValidationResult()
                { Message = $"{move.ToUpper()} is not a valid move - please enter a column A-J and row 1-10, for example A5.", IsMoveValid = false, Move = move.ToUpper() };

            var actual = moveValidatorService.ValidateMove(new MoveValidationQuery(move));

            actual.Should().BeEquivalentTo(expected, $"only letters and numbers are valid characters for a move");
        }

        [Theory]
        [InlineData("1a")]
        public void LetterNotFirstCharacter_Should_ReturnFalseAndCorrectMessage(string move)
        {
            // Sut
            var moveValidatorService = new MoveValidatorService();

            var expected = new MoveValidationResult()
                { Message = $"{move.ToUpper()} is not a valid move - please enter a column A-J and row 1-10, for example A5.", IsMoveValid = false, Move = move.ToUpper() };

            var actual = moveValidatorService.ValidateMove(new MoveValidationQuery(move));

            actual.Should().BeEquivalentTo(expected, $"moves should start with a single letter");
        }

        [Theory]
        [InlineData("x1")]
        public void LetterOutOfRange_Should_ReturnFalseAndCorrectMessage(string move)
        {
            // Sut
            var moveValidatorService = new MoveValidatorService();

            var expected = new MoveValidationResult()
                { Message = $"{move.ToUpper()} is not a valid move - please enter a column A-J and row 1-10, for example A5.", IsMoveValid = false, Move = move.ToUpper() };

            var actual = moveValidatorService.ValidateMove(new MoveValidationQuery(move));

            actual.Should().BeEquivalentTo(expected, $"moves should only contain letters A through J");
        }

        [Theory]
        [InlineData("aa1")]
        public void MoreThanOneLetter_Should_ReturnFalseAndCorrectMessage(string move)
        {
            // Sut
            var moveValidatorService = new MoveValidatorService();

            var expected = new MoveValidationResult()
                { Message = $"{move.ToUpper()} is not a valid move - please enter a column A-J and row 1-10, for example A5.", IsMoveValid = false, Move = move.ToUpper() };

            var actual = moveValidatorService.ValidateMove(new MoveValidationQuery(move));

            actual.Should().BeEquivalentTo(expected, $"moves should only contain one letter");
        }

        [Theory]
        [InlineData("a")]
        public void NoIntegerComponent_Should_ReturnFalseAndCorrectMessage(string move)
        {
            // Sut
            var moveValidatorService = new MoveValidatorService();

            var expected = new MoveValidationResult()
                { Message = $"{move.ToUpper()} is not a valid move - please enter a column A-J and row 1-10, for example A5.", IsMoveValid = false, Move = move.ToUpper() };

            var actual = moveValidatorService.ValidateMove(new MoveValidationQuery(move));

            actual.Should().BeEquivalentTo(expected, $"moves should contain an integer coordinate after the letter");
        }

        [Theory]
        [InlineData("a0")]
        [InlineData("a11")]
        public void IntegerComponentOutOfRange_Should_ReturnFalseAndCorrectMessage(string move)
        {
            // Sut
            var moveValidatorService = new MoveValidatorService();

            var expected = new MoveValidationResult()
                { Message = $"{move.ToUpper()} is not a valid move - please enter a column A-J and row 1-10, for example A5.", IsMoveValid = false, Move = move.ToUpper() };

            var actual = moveValidatorService.ValidateMove(new MoveValidationQuery(move));

            actual.Should().BeEquivalentTo(expected, $"integer component of move should not exceed 10");
        }

        [Theory]
        [InlineData("a1")]
        public void ValidMove_Should_ReturnTrueAndNoMessage(string move)
        {
            // Sut
            var moveValidatorService = new MoveValidatorService();

            var expected = new MoveValidationResult() { IsMoveValid = true, Move = move.ToUpper() };

            var actual = moveValidatorService.ValidateMove(new MoveValidationQuery(move));

            actual.Should().BeEquivalentTo(expected, $"valid move should result in no message and the IsMoveValid field being set true");
        }
    }
}
