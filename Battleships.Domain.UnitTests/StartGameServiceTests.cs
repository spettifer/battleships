﻿using System;
using System.Collections.Generic;
using AutoFixture.Xunit2;
using Battleships.Domain.Commands;
using Battleships.Domain.Models;
using Battleships.Domain.Services;
using Moq;
using Xunit;

namespace Battleships.Domain.UnitTests
{
    public class StartGameServiceTests
    {
        [Fact]
        public void InstantiateWithNoArguments_Should_ThrowArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>("placeShipService", () => new StartGameService(null));
        }

        [Theory]
        [AutoData]
        public void ExcessiveCollisionRetries_Should_ThrowApplicationException(Mock<IPlaceShipService> mockPlaceShipService, StartGameCommand command)
        {
            mockPlaceShipService.Setup(x => x.PlaceShip(It.IsAny<ShipDefinition>(), It.IsAny<int>()))
                .Returns(new HashSet<string>() {"A1", "A2", "A3"});

            var sut = new StartGameService(mockPlaceShipService.Object);

            Assert.Throws<ApplicationException>(() => sut.StartGame(command));
        }
    }
}
