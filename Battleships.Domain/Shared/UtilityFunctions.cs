﻿namespace Battleships.Domain.Shared
{
    public class UtilityFunctions
    {

        /// <summary>
        /// Gets the alpha numeric equivalent of two integer values.
        /// </summary>
        /// <param name="col">The column.</param>
        /// <param name="row">The row.</param>
        /// <returns>A string whereby a col and row of 1 and 5 would be A5.</returns>
        public static string GetAlphaNumericCoordinates(int col, int row)
        {
            return $"{(char)(64 + col)}{row}";
        }
    }
}
