﻿using System;

namespace Battleships.Domain.Shared
{
    public interface IRandomGenerator
    {
        int NextInt(int minValue, int maxValue);
    }

    /// <summary>
    /// This little wrapper allows mocking of the random number generator in tests so that the behaviour can be controlled.
    /// Uses a static Random so that we only have one which should mean we don't end up with repeated numbers.
    /// </summary>
    public class RandomGenerator : IRandomGenerator
    {
        private static Random _rng;

        public RandomGenerator()
        {
            _rng = new Random();
        }

        public int NextInt(int minValue, int maxValue)
        {
            return _rng.Next(minValue, maxValue);
        }
    }
}
