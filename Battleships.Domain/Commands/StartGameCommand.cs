﻿using System.Collections.Generic;
using Battleships.Domain.Models;

namespace Battleships.Domain.Commands
{
    public class StartGameCommand
    {
        public StartGameCommand(IEnumerable<ShipDefinition> ships, int boardSize)
        {
            Ships = ships;
            BoardSize = boardSize;
        }

        public IEnumerable<ShipDefinition> Ships { get; }

        public int BoardSize { get; }
    }
}
