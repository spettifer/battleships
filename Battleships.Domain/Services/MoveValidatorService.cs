﻿using System.Linq;
using System.Text.RegularExpressions;
using Battleships.Domain.Queries;
using Battleships.Domain.Results;

namespace Battleships.Domain.Services
{
    public interface IMoveValidatorService
    {
        MoveValidationResult ValidateMove(MoveValidationQuery query);
    }

    public class MoveValidatorService : IMoveValidatorService
    {
        public MoveValidationResult ValidateMove(MoveValidationQuery query)
        {
            var result = new MoveValidationResult() { IsMoveValid = false };

            var move = query.Move;

            // Is move null/empty?
            if (string.IsNullOrWhiteSpace(move))
            {
                result.Message = "You didn't enter a move. Please try again.";
                result.Move = move;
                return result;
            }

            // The move is a non-null string so ensure the move is upper case.
            move = query.Move.ToUpper();

            result.Move = move;

            if (!move.All(char.IsLetterOrDigit))
            {
                result.Message = $"{move} is not a valid move - please enter a column A-J and row 1-10, for example A5.";
                return result;
            }

            if (!Regex.IsMatch(move.Substring(0, 1), @"^[A-J]+$"))
            {
                result.Message = $"{move} is not a valid move - please enter a column A-J and row 1-10, for example A5.";
                return result;
            }

            var isInteger = int.TryParse(move.Substring(1), out var value);

            if (!isInteger || value < 1 || value > 10)
            {
                result.Message = $"{move} is not a valid move - please enter a column A-J and row 1-10, for example A5.";
                return result;
            }

            // Send back our now validated move (which may have been mutated to upper case).
            result.Move = move;
            result.IsMoveValid = true;

            return result;
        }
    }
}
