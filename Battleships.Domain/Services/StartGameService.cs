﻿using System;
using Battleships.Domain.Commands;
using Battleships.Domain.Models;
using System.Linq;

namespace Battleships.Domain.Services
{
    public interface IStartGameService
    {
        Game StartGame(StartGameCommand command);
    }

    public class StartGameService : IStartGameService
    {
        private readonly IPlaceShipService _placeShipService;

        public StartGameService(IPlaceShipService placeShipService)
        {
            _placeShipService = placeShipService ?? throw new ArgumentNullException(nameof(placeShipService));
        }

        /// <summary>
        /// Starts the game by placing the required ships. Uses the PlaceShipService to get a randomly placed ship of the required size
        /// and then checks if it will collide with a ship or ships that are already placed. If so it will try again until a non-colliding
        /// ship is created. Arbitrarily limits retries to ten  in order to prevent any endless loops in the case of trying to place a
        /// large number of ships.
        /// </summary>
        /// <returns>A populated game board.</returns>
        public Game StartGame(StartGameCommand command)
        {
            var game = new Game();
            game.Board.BoardSize = command.BoardSize;

            foreach (var ship in command.Ships)
            {
                var retryCounter = 0;

                var shipCoordinates = _placeShipService.PlaceShip(ship, command.BoardSize);

                while (game.ShipSegments.Select(s => s.Key).Intersect(shipCoordinates).Any() && retryCounter < 10)
                {
                    // Found a collision so generate a new position for the ship and keep doing it until there are no collisions.
                    shipCoordinates = _placeShipService.PlaceShip(ship, command.BoardSize);
                    retryCounter++;
                }

                if (retryCounter == 10)
                {
                    throw new ApplicationException("Unable to position a ship without collisions inside ten retries.");
                }

                foreach (var coordinates in shipCoordinates)
                {
                    game.ShipSegments.Add(coordinates, new ShipSegment() { Coordinates = coordinates, Hit = false, Parent = ship.FleetIdentifier, Type = ship.Type });
                }
            }

            return game;
        }
    }
}
