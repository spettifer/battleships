﻿using Battleships.Domain.Enums;
using Battleships.Domain.Models;
using Battleships.Domain.Shared;
using System;
using System.Collections.Generic;

namespace Battleships.Domain.Services
{
    public interface IPlaceShipService
    {
        HashSet<string> PlaceShip(ShipDefinition ship, int boardSize);
    }

    public class PlaceShipService : IPlaceShipService
    {
        private readonly IRandomGenerator _randomGenerator;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlaceShipService"/> class.
        /// </summary>
        /// <param name="randomGenerator">The random generator.</param>
        /// <exception cref="ArgumentNullException">randomGenerator</exception>
        public PlaceShipService(IRandomGenerator randomGenerator)
        {
            _randomGenerator = randomGenerator ?? throw new ArgumentNullException(nameof(randomGenerator));
        }

        /// <summary>
        /// Places the ship. Generates pseudo-random starting coordinates and a direction (row or column) and then places the rest of the
        /// ship in the appropriate direction without breaching the bounds of the board size.
        /// </summary>
        /// <param name="ship">The ship.</param>
        /// <param name="boardSize">Size of the board.</param>
        /// <returns>A hashset of the coordinates occupied by the ship.</returns>
        public HashSet<string> PlaceShip(ShipDefinition ship, int boardSize)
        {
            // Ship size should never exceed board size.
            if (ship.Size > boardSize)
            {
                throw new ArgumentOutOfRangeException(nameof(ship), "Ships should not have a size greater than the size of the board.");
            }

            var direction = (Direction)_randomGenerator.NextInt(1, 3);

            var placedShip = new HashSet<string>();

            var startCol = _randomGenerator.NextInt(1, boardSize + 1);
            var startRow = _randomGenerator.NextInt(1, boardSize + 1);
            
            placedShip.Add(UtilityFunctions.GetAlphaNumericCoordinates(startCol, startRow));

            // If our direction value is column then we need to check we won't go outside rows 1 or 10 and vice versa for a direction of row.
            // Therefore, if the start position is (board size - ship size + 1) or less we can count upwards to place the ship as we know we won't
            // exceed row/column 10. If above then count down. This is simplistic but sufficient.
            var startOfShip = direction == Direction.Column ? startRow : startCol;
            var multiplier = startOfShip <= boardSize - ship.Size + 1 ? 1 : -1;
            var limit = ship.Size - 1;

            for (var i = 1; i <= limit; i++)
            {
                var col = direction == Direction.Column ? startCol : startCol + (i * multiplier);
                var row = direction == Direction.Row ? startRow : startRow + (i * multiplier);
                placedShip.Add(UtilityFunctions.GetAlphaNumericCoordinates(col, row));
            }

            return placedShip;
        }   
    }
}
