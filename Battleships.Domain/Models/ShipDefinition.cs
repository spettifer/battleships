﻿using Battleships.Domain.Enums;

namespace Battleships.Domain.Models
{
    public class ShipDefinition
    {
        public ShipType Type { get; set; }

        public int Size { get; set; }

        public int FleetIdentifier { get; set; }
    }
}
