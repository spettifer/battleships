﻿using System.Collections.Generic;

namespace Battleships.Domain.Models
{
    public class Board
    {
        public Dictionary<string, bool> PlayedMoves { get; set; } = new Dictionary<string, bool>();

        public int BoardSize { get; set; }
    }
}
