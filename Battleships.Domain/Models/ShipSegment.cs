﻿using Battleships.Domain.Enums;

namespace Battleships.Domain.Models
{
    public class ShipSegment
    {
        public int Parent { get; set; }

        public string Coordinates { get; set; }

        public ShipType Type { get; set; }

        public bool Hit { get; set; }

    }
}
