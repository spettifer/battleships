﻿using System.Collections.Generic;

namespace Battleships.Domain.Models
{
    public class Game
    {
        public Board Board { get; } = new Board();

        public Dictionary<string, ShipSegment> ShipSegments { get; } = new Dictionary<string, ShipSegment>();

        public bool GameOver { get; set; }
    }
}
