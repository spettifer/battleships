﻿namespace Battleships.Domain.Queries
{
    public class MoveValidationQuery
    {
        public MoveValidationQuery(string move)
        {
            this.Move = move;
        }
        public string Move { get; }
    }
}
