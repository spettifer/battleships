﻿namespace Battleships.Domain.Enums
{
    /// <summary>
    /// Defines the types of ship in the game.
    /// </summary>
    public enum ShipType
    {
        None = 0,
        Battleship = 1,
        Destroyer = 2
    }
}
