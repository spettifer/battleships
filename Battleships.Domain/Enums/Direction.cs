﻿namespace Battleships.Domain.Enums
{
    /// <summary>
    /// Defines the direction in which a ship will be placed.
    /// </summary>
    public enum Direction
    {
        None = 0,
        Row = 1,
        Column = 2
    }
}
