﻿namespace Battleships.Domain.Results
{
    public class MoveValidationResult
    {
        public bool IsMoveValid { get; set; }

        public string Move { get; set; }

        public string Message { get; set; }
    }
}
