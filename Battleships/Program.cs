﻿using System.Reflection;
using Autofac;

namespace Battleships
{
    class Program
    {

        /// <summary>
        /// Provides a root for the DI container to use.
        /// </summary>
        /// <returns>An IContainer</returns>
        private static IContainer CompositionRoot()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Application>();
            builder.RegisterAssemblyTypes(Assembly.GetAssembly(typeof(Domain.AssemblyHook))).AsImplementedInterfaces().InstancePerLifetimeScope();
            return builder.Build();
        }

        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        static void Main()
        {
            // Get an instance of the application to run so we can use DI to do our heavy lifting.
            CompositionRoot().Resolve<Application>().Run();
        }
    }
}
