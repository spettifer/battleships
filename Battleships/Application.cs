﻿using Battleships.Domain.Commands;
using Battleships.Domain.Enums;
using Battleships.Domain.Models;
using Battleships.Domain.Queries;
using Battleships.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Battleships
{
    public class Application
    {
        private readonly IStartGameService _startGameService;
        private readonly IMoveValidatorService _moveValidatorService;

        /// <summary>
        /// Initializes a new instance of the <see cref="Application"/> class.
        /// </summary>
        /// <param name="startGameService">The start game service.</param>
        /// <param name="moveValidatorService">The move validator service.</param>
        public Application(IStartGameService startGameService, IMoveValidatorService moveValidatorService)
        {
            _startGameService = startGameService ?? throw new ArgumentNullException(nameof(startGameService));
            _moveValidatorService = moveValidatorService ?? throw new ArgumentNullException(nameof(moveValidatorService));
        }

        /// <summary>
        /// Runs the game.
        /// </summary>
        public void Run()
        {

            // This is a hard coded list of ship sizes. You could make the list configurable if you wanted to be able to add more ships later.
            // Board size also hard coded here, but again, this could be configurable later. However, you would need to make alterations to the
            // move validation routines to accomodate this and I have not done so for simplicity.
            var command = new StartGameCommand(
                new List<ShipDefinition>()
            {
            new ShipDefinition() { Size = 5, Type = ShipType.Battleship, FleetIdentifier = 1 },
            new ShipDefinition() { Size = 4, Type = ShipType.Destroyer, FleetIdentifier = 2 },
            new ShipDefinition() { Size = 4, Type = ShipType.Destroyer, FleetIdentifier = 3 },
            }, 10);

            var game = _startGameService.StartGame(command);

            while (!game.GameOver)
            {

                Console.WriteLine("Please play a move:");
                var move = Console.ReadLine();

                // Is the move valid?
                var validationResult = _moveValidatorService.ValidateMove(new MoveValidationQuery(move));

                // Get the validated (and possibly mutated to upper case) move.
                move = validationResult.Move;

                if (!validationResult.IsMoveValid)
                {
                    Console.WriteLine(validationResult.Message);
                    continue;
                }

                // If the move has already been played then tell the player and try again.
                if (game.Board.PlayedMoves.ContainsKey(move))
                {
                    Console.WriteLine($"{move} has already been played. Please try again");
                    continue;
                }

                // Get the information of any ship segment at the location of the player's move or null if there is none.
                var shot = game.ShipSegments.ContainsKey(move) ? game.ShipSegments[move] : null;

                var hit = shot != null;

                var afloat = true;

                // If the player hit a ship then update the ship segment to show it.
                if (hit)
                {
                    game.ShipSegments[move].Hit = true;

                    // Is the ship still afloat?
                    afloat = game.ShipSegments.Any(x => !x.Value.Hit && x.Value.Parent == shot.Parent);
                }

                game.Board.PlayedMoves.Add(move, hit);

                Console.WriteLine($"{move} {(hit ? "HIT" : "MISSED.")}{(!afloat ? " and sunk" : string.Empty)}{(hit ? " a " + shot.Type : string.Empty)}");

                if (game.ShipSegments.All(s => s.Value.Hit))
                {
                    game.GameOver = true;
                }
            }

            Console.WriteLine("Congratulations, all enemy ships have been sunk. You win!");

            Console.WriteLine("Please press any key to end the game.");
            Console.ReadKey();

        }
    }
}
