# Battleships

## Notes

### Using a DI Container
This might be considered over-engineering, and it's probably true to an extent when a project is small. However, my argument would be that it's so easy to do and makes unit testing so much easier that there is no reason not to have a habit of adding it. Consider that ASP.Net core includes basic DI by default and it's no bad thing. Small projects can become big projects and it's much simpler if you don't have to retrofit DI, plus tools like ReSharper take a lot of drudge out of writing constructors with parameters. Whilst console apps don't immediately lend themselves to DI, it's not hard to make it work and just because it's a console app there's no reason why you wouldn't want unit tests and to protect the functionality of the application.

### Injected Services
In a bigger, more complex system I would generally try to reduce most things to simple commands and queries which have one purpose only. To do this I tend to use and Interface such as `IServiceQuery<,>` which you implement on the class that does the work, and use a little DI configuration trick that will close these open generics. Each query (in this case) would be unique and you can use simple types or complex types to do it, although of course complex types help you make those signatures more unique. That would definitely be over-engineering in this case though!

### Unit Tests
I'v added quite a few for two of the services but not so many for the StartGameService. I figured adding more by this point won't demonstrate anything extra or useful for this test. I could probably have added less overall but again, by habit I added what I felt was enough to cover the functionality. In a real scenario using some sort of code coverage tool would be favoured to provide metrics which can be part of a CI output to ensure that coverage level remain high where needed.

### General
I considered outputting a simple textual representation of the board after each move but again, this would probably be over-engineering in this context. In retrospect, I might have added a simple command to output a list of played moves and their outcomes.

## Playing The Game
Simply clone the repository, open in Visual Studio (created in VS2017), build in order to restore the required packages and run. Moves can be entered as lower or upper case (e.g. `a5` or `A5`). A certain amount of validation is included, so empty strings, moves which aren't of the form `LetterNumber` or are outside the range of `A-J` and `1-10` will be discarded and the player will be asked to play again. Once a ship has been sunk the player will be informed. Once all ships are sunk, the game ends.